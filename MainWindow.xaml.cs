﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace TileMapEditor
{
    public partial class MainWindow : Window
    {
        Loader loader = new Loader(@"C:\Levels.txt");
        public MainWindow()
        {
            InitializeComponent();
            Loaded += LoadLevels;
        }

        private void LoadLevels(object sender, RoutedEventArgs e) 
        {
            loader.LoadLevels();

            DrawLevel(1);
        }

        public void DrawLevel(int LevelNumber)
        {
            Tile[,] currentLevel = loader.GetLevelByIndex(LevelNumber);

            for (int i = 0; i < 13; i++)
            {
                for (int j = 0; j < 13; j++)
                {
                    DrawImage(currentLevel[i,j], i,j);
                }
            }
        }

        private void DrawImage(Tile tile, int x, int y)
        {
            Image image = new Image();

            switch (tile)
            {
                case Tile.wall:
                    image.Source = App.bitmapWall;
                    break;
                case Tile.none:
                    image.Source = App.bitmapNone;
                    break;
                default:
                    image.Source = App.bitmapNone;
                    break;
            }

            ContentGrid.Children.Add(image);
            Grid.SetRow(image, x);
            Grid.SetColumn(image, y);
        }
    }
}
