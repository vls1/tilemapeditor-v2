﻿using System;
using System.Windows;
using System.IO;
using System.Windows.Media.Imaging;

namespace TileMapEditor
{
    public partial class App : Application
    {
        public static BitmapImage bitmapNone = GetImage("None.png");
        public static BitmapImage bitmapWall = GetImage("Brick_Wall.png");

        private static BitmapImage GetImage(string fileName)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "Recources", fileName);
            Uri uri = new Uri(path);
            return new BitmapImage(uri);
        }
    }
}
